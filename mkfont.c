#include <ft2build.h>
#include FT_FREETYPE_H

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

struct tinyfont {
	uint32_t count;         /* number of glyphs */
	uint32_t height, width; /* glyph width/height */
	uint32_t *code;         /* glyph character code (UTF-8) */
	uint8_t  *bitmap;       /* glyph bitmap */
};

void bmp_copy(FT_GlyphSlot glyph, struct tinyfont *f)
{
	uint8_t *dest;
	int i, width, height, xoff, yoff;
	FT_Bitmap *bmp = &glyph->bitmap;

       	dest = f->bitmap + f->count * f->width * f->height;
	yoff = MAX(0, (int)f->height - glyph->bitmap_top - 7);
	xoff = glyph->bitmap_left;
	height = MIN(f->height - yoff, bmp->rows);
	width = MIN(f->width - xoff, bmp->width);

	for (i = 0; i < height; i++)
		memcpy(dest + xoff + (i + yoff) * f->width, bmp->buffer + i * bmp->width, width);
}

int main(int argc, char **argv)
{
	int opt, fd;
	uint32_t i, idx;
	struct tinyfont f = {0};
	FT_Library lib;
	FT_Face face;

	while ((opt = getopt(argc, argv, "w:h:")) != -1) {
		switch (opt) {
		case 'w':
			f.width = atoi(optarg);
			break;
		case 'h':
			f.height = atoi(optarg);
			break;
		}
	}

	if (!f.width || !f.height || !argv[optind])
		errx(1, "usage: %s -w <width> -h <height> <font.ttf>", argv[0]);

	if (FT_Init_FreeType(&lib))
		err(1, "FT_Init_FreeType");

	if (FT_New_Face(lib, argv[optind], 0, &face))
		err(1, "FT_New_Face");

	if (FT_Set_Char_Size(face, 0, 10*64, 100, 100))
		err(1, "FT_Set_Char_Size");

	/*if (FT_Set_Pixel_Sizes(face, f.width, f.height))
		err(1, "FT_Set_Pixel_Sizes");*/

	f.code = malloc(face->num_glyphs * sizeof(*f.code));
	f.bitmap = calloc(face->num_glyphs, f.width * f.height);
	if (!f.code || !f.bitmap)
		err(1, "malloc");

	for (i = 0; i < 0xffff; i++) {
		if ((idx = FT_Get_Char_Index(face, i)) == 0)
			continue;
		if (FT_Load_Glyph(face, idx, FT_LOAD_RENDER))
			err(1, "FT_Load_Glyph");
		if (FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL))
			err(1, "FT_Render_Glyph");

		bmp_copy(face->glyph, &f);
		f.code[f.count++] = i;
	}

	if ((fd = creat("font.tf", 0600)) < 0)
		err(1, "open font.tf");
	write(fd, &f, 24);
	write(fd, f.code, f.count * sizeof(*f.code));
	write(fd, f.bitmap, f.count * f.width * f.height);
	close(fd);
}
