#include <ctype.h>
#include <err.h>
#include <pty.h>
#include <poll.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <linux/fb.h>
#include <linux/vt.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/wait.h>

#define BGCOLOR 0x111111 /* Default background color */
#define FGCOLOR 0xCCCCCC /* Default foreground color */
#define CR(c) ((c >> 16) & 0xff) /* Extract red */
#define CG(c) ((c >> 8)  & 0xff) /* Extract green */
#define CB(c) ((c >> 0)  & 0xff) /* Extract blue */
#define COLORMERGE(f, b, c) ((b) + (((f) - (b)) * (c) >> 8))

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#define MAX(a, b)  ((a) < (b) ? (b) : (a))
#define MIN(a, b)  ((a) < (b) ? (a) : (b))
#define LIMIT(n, a, b)  ((n) < (a) ? (a) : ((n) > (b) ? (b) : (n)))

#define ATTR_BOLD	0x01
#define ATTR_REVERSE	0x02

#define MODE_CURSOR	0x01

struct cell {
	uint32_t c;      /* character code */
	uint32_t fg, bg; /* foreground/background color */
};

struct term {
	struct cell *screen; /* screen content */
	char *dirty;         /* redraw line if set */
	uint scroll;         /* circular buffer scroll offset */
	uint width, height;  /* screen width/height */
	uint curx, cury;     /* cursor position */
	uint top, bot;       /* scrolling region */
	uint attr, mode;     /* attributes, mode */
	uint32_t fg, bg;     /* current foreground/background color */
	int fd;              /* program file descriptor */
	int hidden;          /* do not draw if not 0 */
	pid_t pid;           /* program pid */
};

struct font {
	uint count;		/* number of glyphs */
	uint height, width;	/* glyph height/width */
	uint *glyph;		/* glyph charater codes */
	uint8_t *data;		/* glyph bitmaps */
};

struct fbparam {
	int fd;             /* /dev/fbX file descriptor */
	uint32_t *ptr;      /* framebuffer mmap */
	size_t size;        /* mmap size */
	uint width, height; /* framebuffer width/height */
	struct fb_var_screeninfo vinfo;
	struct fb_fix_screeninfo finfo;
};

static struct term term_list[1];		/* Terminals list */
static struct term *term = &term_list[0];	/* Pointer to current terminal */
static struct font font;
static struct fbparam fb;

static int term_getc(void)
{
	static char buf[4096];
	static ssize_t len = 0, cur = 0;
	struct pollfd fds = {.fd = term->fd, .events = POLLIN};

	if (cur < len)
		return buf[cur++];

	if (poll(&fds, 1, 1) <= 0)
		return -1;

	if ((len = read(term->fd, buf, sizeof(buf))) <= 0)
		return -1;

	cur = 1;
	return buf[0];
}

static struct cell *term_cell(uint x, uint y)
{
	y = (term->scroll + y) % term->height;
	return term->screen + x + y * term->width;
}

static void term_dirty(uint sy, uint dy)
{
	for (; sy <= dy; sy++)
		term->dirty[sy] = 1;
}

static void term_put(uint x, uint y, int chr)
{
	struct cell *c = term_cell(x, y);
	c->fg = term->attr & ATTR_REVERSE ? term->bg : term->fg;
	c->bg = term->attr & ATTR_REVERSE ? term->fg : term->bg;
	c->c = chr;
	term->dirty[y] = 1;
}

static void term_blank(uint sx, uint sy, uint dx, uint dy)
{
	uint x, y;
	for (y = sy; y < dy; y++)
		for (x = sx; x < dx; x++)
			term_put(x, y, 0);
}

static void term_scroll_up(uint n, uint top, uint bot)
{
	n = LIMIT(n, 1, bot - top);
	term->scroll = (term->scroll + n) % term->height;
	term_dirty(top, bot);
	term_blank(0, bot - n + 1, term->width, bot + 1);
}

static void term_scroll_down(uint n, uint top, uint bot)
{
	n = LIMIT(n, 1, bot - top);
	term->scroll = term->scroll < n ? term->height-1 : term->scroll - n;
	term_dirty(top, bot);
	term_blank(0, top, term->width, top + n);
}

static void cursor_move(int x, int y)
{
	term->dirty[term->cury] = 1;
	term->curx = MAX(0, x);
	term->cury = MAX(0, y);
	if (term->curx >= term->width) {
		term->curx = 0;
		term->cury++;
	}
	if (term->cury > term->bot) {
		term_scroll_up(1, term->top, term->bot);
		term->cury = term->bot;
	}
	term->dirty[term->cury] = 1;
}

static void term_reset(void)
{
	term->fg = FGCOLOR;
	term->bg = BGCOLOR;
	term->mode = MODE_CURSOR;
	term->attr = 0;
	term->top = 0;
	term->bot = term->height - 1;
	cursor_move(0, 0);
	term_blank(0, 0, term->width, term->height);
}

/* Sequences from https://www.inwap.com/pdp10/ansicode.txt */
static void sgrseq(int n)
{
	const uint32_t colormap[16] = {
		0x000000, 0xaa0000, 0x00aa00, 0xaa5500, 0x0000aa, 0xaa00aa, 0x00aaaa, 0xaaaaaa,
	};

	switch(n) {
	case 0:		/* Reset all attributes to their defaults */
		term->fg = FGCOLOR;
		term->bg = BGCOLOR;
		term->attr = 0;
		break;
	case 1:		/* Turn on bold */
		term->attr |= ATTR_BOLD;
		break;
	case 7:		/* Reverse video (swap foreground and background colors) */
		term->attr |= ATTR_REVERSE;
		break;
	case 27:	/* Turn off reverse video */
		term->attr &= ~ATTR_REVERSE;
		break;
	case 30 ... 37:	/* Set foreground color */
		term->fg = colormap[n - 30];
		break;
	case 39:	/* Set default foreground color */
		term->fg = FGCOLOR;
		break;
	case 40 ... 47:	/* Set background color */
		term->bg = colormap[n - 40] ;
		break;
	case 49:	/* Set default background color */
		term->bg = BGCOLOR;
		break;
	}
	if (term->attr & ATTR_BOLD)
		term->fg = term->fg == FGCOLOR ? 0xffffff : term->fg + 0x555555;
}

static void decprivseq(int c)
{
	uint param = 0, mode = 0;
	while (isdigit(c)) {
		param = param * 10 + (c - '0');
		c = term_getc();
	}

	switch(param) {
	case 1:
		write(1, c == 'l' ? "\033[?1l" : "\033[?1h", 5);
		break;
	case 25:	/* hide/show cursor */
		mode = MODE_CURSOR;
		break;
	}
	if (c == 'l')
		term->mode &= ~mode;
	else if (c == 'h')
		term->mode |= mode;
}

static void csiseq(int c)
{
	uint param[16] = {0}, count = 0, n;
	do {
		n = 0;
		while (isdigit(c)) {
			n = n * 10 + (c - '0');
			c = term_getc();
		}
		param[count++] = n;
		if (c == ';')
			c = term_getc();
		else
			break;
	} while (count < ARRAY_SIZE(param));

	switch (c) {
	case '?':	/* DEC private mode */
		decprivseq(term_getc());
		break;
	case 'A':	/* CUU - move cursor up */
		cursor_move(term->curx, term->cury - MAX(1, param[0]));
		break;
	case 'B':	/* CUD - move cursor down */
	case 'e':
		cursor_move(term->curx, term->cury + MAX(1, param[0]));
		break;
	case 'C':	/* CUF - move cursor right */
	case 'a':
		cursor_move(term->curx + MAX(1, param[0]), term->cury);
		break;
	case 'D':	/* CUB - move cursor left */
		cursor_move(term->curx - MAX(1, param[0]), term->cury);
		break;
	case 'E':	/* CNL - move cursor down, x = 0 */
		cursor_move(0, MAX(1, term->cury + param[0]));
		break;
	case 'F':	/* CPL - move cursor up, x = 0 */
		cursor_move(0, MAX(1, term->cury - param[0]));
		break;
	case 'G':	/* CHA - move cursor to x, y = cury */
		cursor_move(MAX(1, param[0]) - 1, term->cury);
		break;
	case 'H':	/* CUP - move cursor to y, x (origin at 1, 1) */
	case 'f':
		cursor_move(MAX(1, param[1]) - 1, MAX(1, param[0]) - 1);
		break;
	case 'J':       /* ED - erase display */
		switch (param[0]) {
		case 0: /* From cursor to end of display */
			term_blank(term->curx, term->cury, term->width, term->cury + 1);
			term_blank(0, term->cury + 1, term->width, term->height);
			break;
		case 1: /* From start to cursor */
			term_blank(0, 0, term->width, term->cury - 1);
			term_blank(0, term->cury, term->curx + 1, term->cury + 1);
			break;
		case 2: /* Whole display */
		case 3: /* Whole display including scroll-back */
			term_blank(0, 0, term->width, term->height);
			break;
		}
		break;
	case 'K':       /* EL - erase line */
		switch (param[0]) {
		case 0: /* From cursor to end of line */
			term_blank(term->curx, term->cury, term->width, term->cury + 1);
			break;
		case 1: /* From start of line to cursor */
			term_blank(0, term->cury, term->curx + 1, term->cury + 1);
			break;
		case 2: /* Whole line */
			term_blank(0, term->cury, term->width, term->cury + 1);
			break;
		}
		break;
	case 'L':	/* IL - insert line (scroll down) */
		term_scroll_down(MAX(1, param[0]), MIN(term->cury, term->bot), term->bot);
		break;
	case 'M':	/* DL - delete line (scroll up) */
		term_scroll_up(MAX(1, param[0]), MIN(term->cury, term->bot), term->bot);
		break;
	case 'c':	/* Answer ESC [ 6 c */
		write(term->fd, "\033[?6c", 5);
		break;
	case 'd':	/* Move cursor to y, x = curx */
		cursor_move(term->curx, MAX(1, param[0]) - 1);
		break;
	case 'm':	/* SGR - Set graphics rendition */
		for (n = 0; n < count; n++)
			sgrseq(param[n]);
		break;
	case 'r':	/* Set scroll region */
		term->top = MIN(MAX(1, param[0]) - 1, term->height - 3);
		term->bot = LIMIT(MAX(1, param[1]) - 1, term->top + 2, term->height - 1);
		break;
	}
}

static void escseq(int c)
{
	switch (c) {
	case 'c':	/* RIS - reset */
		term_reset();
		break;
	case 'D':	/* IND - Linefeed */
		cursor_move(term->curx, term->cury + 1);
		break;
	case 'E':	/* NEL - Newline */
		cursor_move(0, term->cury + 1);
		break;
	case 'M':	/* RI - reverse linefeed */
		cursor_move(term->curx, term->cury - 1);
		break;
	case '[':	/* CSI - control sequence introducer */
		csiseq(term_getc());
		break;
	}
}

static void cntrlseq(int c)
{
	switch (c) {
	case 0x08:	/* BS - backspace */
		cursor_move(term->curx - 1, term->cury);
		break;
	case 0x09:	/* HT - horizontal tab to next tab stop */
		term_blank(term->curx, term->cury,
		           term->curx + 8 - term->curx % 8, term->cury + 1);
		cursor_move(term->curx + 8 - term->curx % 8, term->cury);
		break;
	case 0x0a:	/* LF - new line */
	case 0x0b:	/* VT - vertical tab */
	case 0x0c:	/* FF - form feed */
		cursor_move(term->curx, term->cury + 1);
		break;
	case 0x0d:	/* CR - carriage return */
		cursor_move(0, term->cury);
		break;
	case 0x1b:	/* ESC - escape sequence start */
		escseq(term_getc());
		break;
	case 0x9b:	/* CSI - equivalent to ESC [ */
		csiseq(term_getc());
		break;
	}
}

static uint8_t *font_glyph(uint c)
{
	uint mid, low = 0, high = font.count;
	while (low < high) {
		mid = (low + high) / 2;
		if (font.glyph[mid] == c)
			return font.data + mid * font.width * font.height;
		if (c < font.glyph[mid])
			high = mid;
		else
			low = mid + 1;
	}
	return NULL;
}

static uint32_t pixcolor(uint8_t r, uint8_t g, uint8_t b)
{
	struct fb_var_screeninfo *v = &fb.vinfo;
	return (r << v->red.offset) | (g << v->green.offset) | (b << v->blue.offset);
}

static uint32_t *fbgetaddr(uint x, uint y)
{
	return fb.ptr + x + fb.vinfo.xoffset + (y + fb.vinfo.yoffset) * fb.width;
}

static void draw_glyph(uint dx, uint dy, struct cell *c)
{
	uint x, y;
	uint32_t *p, pix;
	uint8_t *glyph;
	uint8_t fg[3] = {CR(c->fg), CG(c->fg), CB(c->fg)};
	uint8_t bg[3] = {CR(c->bg), CG(c->bg), CB(c->bg)};

	if (!c->c || c->c == ' ' || !(glyph = font_glyph(c->c))) {
		pix = pixcolor(bg[0], bg[1], bg[2]);
		for (y = 0; y < font.height; y++) {
			p = fbgetaddr(dx, y+dy);
			for (x = 0; x < font.width; x++)
				*p++ = pix;
		}
		return;
	}

	for (y = 0; y < font.height; y++) {
		p = fbgetaddr(dx, y+dy);
		for (x = 0; x < font.width; x++, glyph++) {
			*p++ = pixcolor(COLORMERGE(fg[0], bg[0], *glyph),
					COLORMERGE(fg[1], bg[1], *glyph),
					COLORMERGE(fg[2], bg[2], *glyph));
		}
	}
}

static void term_draw(void)
{
	uint x, y;
	struct cell *c, cursor;

	if (term->hidden)
		return;

	for (y = 0; y < term->height; y++) {
		if (!term->dirty[y])
			continue;
		for (x = 0; x < term->width; x++)
			draw_glyph(x*font.width, y*font.height, term_cell(x, y));
		term->dirty[y] = 0;
	}
	if (term->mode & MODE_CURSOR) {
		c = term_cell(term->curx, term->cury);
		cursor = (struct cell){.c = c->c, .fg = c->bg, .bg = c->fg};
		draw_glyph(term->curx*font.width, term->cury*font.height, &cursor);
	}
}

static int utf8_read(int *chr)
{
	uint32_t c, code, len = 1;

	c = term_getc();
	if (~c & 0xC0 || (int) c == -1)
		return *chr = c, 0;
	while (len < 4 && c & (0x40 >> len))
		len++;

	code = (0x3f >> len) & c;
	for (; len; len--) {
		c = (uint8_t) term_getc();
		if (c >> 6 != 2)
			break;
		code = (code << 6) | (c & 0x3f);
	}
	*chr = len > 0 ? c : code;
	return len;
}

static void loop(void)
{
	int c = -1;
	char buf[8];
	ssize_t n;
	struct pollfd fds[] = {
		{.fd = 0, .events = POLLIN},
		{.fd = term->fd, .events = POLLIN}
	};

	for (;;) {
		term_draw();
		if (c == -1 && poll(fds, ARRAY_SIZE(fds), -1) < 0)
			continue;
		if (fds[1].revents & (POLLHUP | POLLERR | POLLNVAL))
			break;
		if ((n = read(0, buf, sizeof(buf))) > 0)
			write(term->fd, buf, n);
		for (n = 0; n < 65536; n++) {
			if (utf8_read(&c)) {
				/* invalid sequence */
				term_put(term->curx, term->cury, 0xFFFD);
				cursor_move(term->curx + 1, term->cury);
			}
			if (c == -1)
				break;
			if (iscntrl(c))
				cntrlseq(c);
			else {
				term_put(term->curx, term->cury, c);
				cursor_move(term->curx + 1, term->cury);
			}
		}
	}
}

static void term_init(char **argv)
{
	term->width = fb.width / font.width;
	term->height = fb.height / font.height;
	term->screen = malloc(term->width * term->height * sizeof(*term->screen));
	term->dirty = malloc(term->height * sizeof(*term->dirty));
	if (!term->screen || !term->dirty)
		err(1, "malloc");
	term_reset();

	struct winsize winp = {.ws_row = term->height, .ws_col = term->width};
	term->pid = forkpty(&term->fd, NULL, NULL, &winp);
	if (!term->pid) {
		execvp(argv[0], argv);
		err(1, "execvp");
	} else if (term->pid == -1)
		err(1, "forkpty");
}

static void sighandler(int n)
{
	switch (n) {
		case SIGUSR1:
			term->hidden = 1;
			ioctl(0, VT_RELDISP, 1);
			break;
		case SIGUSR2:
			term->hidden = 0;
			term_dirty(0, term->height - 1);
			term_draw();
			break;
		case SIGCHLD:
			while (waitpid(-1, NULL, WNOHANG) > 0);
			break;
	}
}

static void signal_init(void)
{
	struct vt_mode vtm = {VT_PROCESS, 0, SIGUSR1, SIGUSR2, 0};
	signal(SIGUSR1, sighandler);
	signal(SIGUSR2, sighandler);
	signal(SIGCHLD, sighandler);
	ioctl(0, VT_SETMODE, &vtm);
}

static void font_init()
{
	extern char _binary_font_tf_start[];
	void *p = _binary_font_tf_start;
	memcpy(&font, p, 12);			/* Copy glyphs count, width and height */
	font.glyph = p + 12;
	font.data = p + 12 + (font.count * sizeof(uint));
}

static void fbclose(void)
{
	munmap(fb.ptr, fb.size);
	close(fb.fd);
}

static void fb_init(const char *path)
{
	if ((fb.fd = open(path, O_RDWR | O_CLOEXEC)) < 0)
		err(1, "open %s", path);

	if (ioctl(fb.fd, FBIOGET_VSCREENINFO, &fb.vinfo) < 0 ||
	    ioctl(fb.fd, FBIOGET_FSCREENINFO, &fb.finfo) < 0)
		err(1, "ioctl");

	fb.width = fb.vinfo.xres_virtual;
	fb.height = fb.vinfo.yres_virtual;
	fb.size = fb.finfo.line_length * fb.vinfo.yres_virtual;
	fb.ptr = mmap(NULL, fb.size, PROT_WRITE, MAP_SHARED, fb.fd, 0);
	if (fb.ptr == MAP_FAILED)
		err(1, "mmap");
}

int main(int argc, char **argv)
{
	struct termios oldtios, newtios;

	if (argc < 2)
		errx(1, "usage: %s <program>", argv[0]);
	fb_init("/dev/fb0");
	font_init();
	signal_init();
	term_init(&argv[1]);

	tcgetattr(0, &oldtios);
	cfmakeraw(&newtios);
	tcsetattr(0, TCSAFLUSH, &newtios);
	fcntl(0, F_SETFL, O_NONBLOCK);
	write(1, "\033[?25l", 6);
	loop();
	write(1, "\033[?25h", 6);

	tcsetattr(0, 0, &oldtios);
	fbclose();
}
