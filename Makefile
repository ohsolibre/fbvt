CC = cc
CFLAGS = -O2 -Wall -Wextra
LDFLAGS = -static -s

all: fbvt

fbvt: fbvt.c font.o
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

mkfont: mkfont.c
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) `pkg-config --cflags --libs freetype2`

font.o: font.tf
	ld -r -b binary -o $@ $<
	objcopy --rename-section .data=.rodata,CONTENTS,ALLOC,LOAD,READONLY,DATA $@

font.tf:
	./mkfont -w 10 -h 20 DejaVuSansMono.ttf

clean:
	rm -f fbvt font.o mkfont
